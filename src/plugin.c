/* PipeQ
 *
 * Copyright © 2021 Fabian Hügel
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <lilv-0/lilv/lilv.h>
#include <lv2/atom/atom.h>
#include <lv2/options/options.h>
#include <lv2/urid/urid.h>
#include "array.h"
#include "plugin.h"

static const uint32_t MINIMUM_BLOCK_LENGTH = 1;
static const uint32_t NOMINAL_BLOCK_LENGTH = 1024;
static const uint32_t MAXIMUM_BLOCK_LENGTH = 2048;

enum port_type {
	TYPE_UNKNOWN,
	TYPE_CONTROL,
	TYPE_AUDIO,
	TYPE_ATOM,
	TYPE_CONTROL_ARRAY,
} __attribute__ ((__packed__));

enum port_direction {
	DIRECTION_UNKNOWN,
	DIRECTION_INPUT,
	DIRECTION_OUTPUT,
} __attribute__ ((__packed__));

struct port {
	enum port_type type;

	enum port_direction direction;

	float min;
	float max;
	float def;

	unsigned int is_enum : 1;
	unsigned int is_toggle : 1;
	unsigned int is_optional : 1;
	unsigned int is_integer : 1;

	unsigned int is_side_chain : 1;

	LilvScalePoints *scale_points;

	union {
		float control;
		LV2_Atom atom;
	} port_data;
};

struct plugin {
	const LilvPlugin *lilv_plugin;
	LilvInstance *lilv_instance;
	bool active;
	uint32_t input_port_indices[2];
	uint32_t output_port_indices[2];
	uint32_t n_ports;
	struct port ports[];
};

static LilvWorld *world;
static const LilvPlugins *plugins;

static float db_to_factor(float db)
{
	return exp((db / 20.0) * log(10.0));
}

void plugin_reset_ports(struct plugin *plugin)
{
	for (uint32_t i = 0; i < plugin->n_ports; i++) {
		struct port *port = &plugin->ports[i];
		if (port->type == TYPE_CONTROL && port->direction == DIRECTION_INPUT) {
			assert(isfinite(port->def));
			assert(port->def >= port->min && port->def <= port->max);
			port->port_data.control = port->def;
		} else {
			memset(&port->port_data, 0, sizeof(port->port_data));
		}
	}
}

static bool initialize_port(struct port *port, const LilvPort *lilv_port, const LilvPlugin *lilv_plugin)
{
	enum port_type type = TYPE_UNKNOWN;
	enum port_direction direction = DIRECTION_UNKNOWN;
	bool __attribute__((unused)) strict_bounds = false;
	bool __attribute__((unused)) enumeration = false;
	bool __attribute__((unused)) integer = false;
	bool __attribute__((unused)) logarithmic = false;
	bool __attribute__((unused)) reports_latency = false;
	bool __attribute__((unused)) optional = false;
	bool __attribute__((unused)) toggled = false;
	bool __attribute__((unused)) not_automatic = false;
	bool __attribute__((unused)) momentary_trigger = false;
	bool __attribute__((unused)) is_side_chain = false;
	bool __attribute__((unused)) not_on_gui = false;
	bool __attribute__((unused)) non_automable = false;
	bool __attribute__((unused)) expensive = false;
	const LilvNodes *classes = lilv_port_get_classes(lilv_plugin, lilv_port);
	LILV_FOREACH(nodes, iter, classes) {
		const LilvNode *node = lilv_nodes_get(classes, iter);
		const char *class = lilv_node_as_string(node);
		if (strcmp(class, "http://lv2plug.in/ns/lv2core#InputPort") == 0) {
			direction = DIRECTION_INPUT;
		} else if (strcmp(class, "http://lv2plug.in/ns/lv2core#OutputPort") == 0) {
			direction = DIRECTION_OUTPUT;
		} else if (strcmp(class, "http://lv2plug.in/ns/lv2core#AudioPort") == 0) {
			type = TYPE_AUDIO;
		} else if (strcmp(class, "http://lv2plug.in/ns/lv2core#ControlPort") == 0) {
			type = TYPE_CONTROL;
		} else if (strcmp(class, "http://lv2plug.in/ns/ext/atom#AtomPort") == 0) {
			type = TYPE_ATOM;
		} else if (strcmp(class, "http://lv2plug.in/ns/lv2core#CVPort") == 0) {
			type = TYPE_CONTROL_ARRAY;
		} else {
			fprintf(stderr, "Warning: unknown port class: %s\n", class);
		}
	}
	LilvNodes *properties = lilv_port_get_properties(lilv_plugin, lilv_port);
	LILV_FOREACH(nodes, iter, properties) {
		const LilvNode *node = lilv_nodes_get(properties, iter);
		const char *property = lilv_node_as_string(node);
		if (strcmp(property, "http://lv2plug.in/ns/ext/port-props#hasStrictBounds") == 0) {
			strict_bounds = true;
		} else if (strcmp(property, "http://lv2plug.in/ns/lv2core#enumeration") == 0) {
			enumeration = true;
		} else if (strcmp(property, "http://lv2plug.in/ns/lv2core#integer") == 0) {
			integer = true;
		} else if (strcmp(property, "http://lv2plug.in/ns/ext/port-props#logarithmic") == 0) {
			logarithmic = true;
		} else if (strcmp(property, "http://lv2plug.in/ns/lv2core#reportsLatency") == 0) {
			reports_latency = true;
		} else if (strcmp(property, "http://lv2plug.in/ns/lv2core#connectionOptional") == 0) {
			optional = true;
		} else if (strcmp(property, "http://lv2plug.in/ns/lv2core#toggled") == 0) {
			toggled = true;
		} else if (strcmp(property, "http://lv2plug.in/ns/ext/port-props#notAutomatic") == 0) {
			not_automatic = true;
		} else if (strcmp(property, "http://lv2plug.in/ns/ext/port-props#trigger") == 0) {
			momentary_trigger = true;
		} else if (strcmp(property, "http://lv2plug.in/ns/lv2core#isSideChain") == 0) {
			is_side_chain = true;
		} else if (strcmp(property, "http://lv2plug.in/ns/ext/port-props#notOnGUI") == 0) {
			not_on_gui = true;
		} else if (strcmp(property, "http://kxstudio.sf.net/ns/lv2ext/props#NonAutomable") == 0) {
			non_automable = true;
		} else if (strcmp(property, "http://lv2plug.in/ns/ext/port-props#expensive") == 0) {
			expensive = true;
		} else {
			fprintf(stderr, "Warning: unknown port property: %s\n", property);
		}
	}
	lilv_nodes_free(properties);

	if (type == TYPE_UNKNOWN) {
		fputs("Unknown port type\n", stderr);
		return false;
	}

	if (direction == DIRECTION_UNKNOWN) {
		fputs("Unknown port direction\n", stderr);
		return false;
	}

	if (type == TYPE_CONTROL_ARRAY) {
		fputs("Control array not implemented\n", stderr);
		return false;
	}

	if (is_side_chain && !optional) {
		fputs("Side chain not implemented\n", stderr);
		return false;
	}

	if (type == TYPE_ATOM && direction == DIRECTION_INPUT && !optional) {
		fputs("Non-optional input atom not implemented\n", stderr);
		return false;
	}

	if (toggled && enumeration) {
		fputs("Error: port is both toggle and enum\n", stderr);
		return false;
	}

	float def = NAN;
	float min = NAN;
	float max = NAN;
	if (type == TYPE_CONTROL) {
		LilvNode *def_node, *min_node, *max_node;
		lilv_port_get_range(lilv_plugin, lilv_port, &def_node, &min_node, &max_node);
		def = def_node ? lilv_node_as_float(def_node) : NAN;
		min = min_node ? lilv_node_as_float(min_node) : -INFINITY;
		max = max_node ? lilv_node_as_float(max_node) : INFINITY;
		lilv_node_free(def_node);
		lilv_node_free(min_node);
		lilv_node_free(max_node);
	}

	port->type = type;
	port->direction = direction;
	port->def = def;
	port->min = min;
	port->max = max;
	port->is_enum = enumeration;
	port->is_toggle = toggled;
	port->is_integer = integer;
	port->is_optional = optional;
	port->is_side_chain = is_side_chain;
	port->scale_points = lilv_port_get_scale_points(lilv_plugin, lilv_port);

	return true;
}

static bool initialize_ports(struct plugin *plugin)
{
	int64_t in_indices[2] = {-1, -1};
	int64_t out_indices[2] = {-1, -1};
	for (uint32_t i = 0; i < plugin->n_ports; i++) {
		const LilvPort *lilv_port = lilv_plugin_get_port_by_index(plugin->lilv_plugin, i);
		assert(lilv_port);
		struct port *port = &plugin->ports[i];
		if (!initialize_port(port, lilv_port, plugin->lilv_plugin)) {
			return false;
		}
		if (port->type != TYPE_AUDIO) {
			continue;
		}
		if (port->is_side_chain) {
			assert(port->is_optional);
			continue;
		}
		int64_t *indices = port->direction == DIRECTION_INPUT ? in_indices : out_indices;
		unsigned int channel = 0;
		if (indices[0] != -1) {
			channel = 1;
			if (indices[1] != -1) {
				if (port->is_optional) {
					continue;
				}
				fprintf(stderr, "Plugins with more than 2 audio %s channels are not supported\n",
					port->direction == DIRECTION_INPUT ? "input" : "output");
				return false;
			}
		}
		indices[channel] = i;
	}

	if (in_indices[0] == -1 || in_indices[1] == -1) {
		fputs("Plugins with less than 2 audio input channels are not supported\n", stderr);
		return false;
	}
	if (out_indices[0] == -1 || out_indices[1] == -1) {
		fputs("Plugins with less than 2 audio output channels are not supported\n", stderr);
		return false;
	}
	plugin->input_port_indices[0] = in_indices[0];
	plugin->input_port_indices[1] = in_indices[1];
	plugin->output_port_indices[0] = out_indices[0];
	plugin->output_port_indices[1] = out_indices[1];

	plugin_reset_ports(plugin);

	for (uint32_t i = 0; i < plugin->n_ports; i++) {
		struct port *port = &plugin->ports[i];
		if (port->direction == DIRECTION_OUTPUT && port->is_optional) {
			continue;
		}
		if (port->type == TYPE_ATOM && port->is_optional) {
			continue;
		}
		lilv_instance_connect_port(plugin->lilv_instance, i, &port->port_data);
	}
	return true;
}

static LV2_URID map_urid(LV2_URID_Map_Handle handle, const char *uri);

struct plugin *plugin_create(const char *plugin_uri, double sample_rate)
{
	LilvNode *node = lilv_new_uri(world, plugin_uri);
	const LilvPlugin *lilv_plugin = lilv_plugins_get_by_uri(plugins, node);
	lilv_node_free(node);
	if (!lilv_plugin) {
		fprintf(stderr, "Plugin '%s' not found\n", plugin_uri);
		return NULL;
	}

	LilvNodes *required = lilv_plugin_get_required_features(lilv_plugin);
	bool requires_unsupported_features = false;
	LILV_FOREACH(nodes, i, required) {
		const LilvNode *node = lilv_nodes_get(required, i);
		const char *uri = lilv_node_as_uri(node);
		if (strcmp(uri, LV2_URID__map) == 0) {
		} else if (strcmp(uri, "http://lv2plug.in/ns/ext/options#options") == 0) {
		} else if (strcmp(uri, "http://lv2plug.in/ns/ext/buf-size#boundedBlockLength") == 0) {
		} else if (strcmp(uri, "http://lv2plug.in/ns/ext/port-props#supportsStrictBounds") == 0) {
		} else {
			fprintf(stderr, "Plugin '%s' requires feature '%s' which is not implemented\n",
				plugin_uri, uri);
			requires_unsupported_features = true;
		}
	}
	lilv_nodes_free(required);
	if (requires_unsupported_features) {
		return NULL;
	}

	const LV2_URID_Map urid_map = {
		.handle = NULL,
		.map = map_urid,
	};
	const LV2_Feature urid_map_feature = {
		.URI = LV2_URID__map,
		.data = (void *)&urid_map,
	};

	const LV2_Options_Option options[] = {
		{
			.context = LV2_OPTIONS_INSTANCE,
			.key = map_urid(NULL, "http://lv2plug.in/ns/ext/buf-size#nominalBlockLength"),
			.size = sizeof(uint32_t),
			.type = map_urid(NULL, "http://lv2plug.in/ns/ext/atom#Int"),
			.value = &NOMINAL_BLOCK_LENGTH,
		},
		{
			.context = LV2_OPTIONS_INSTANCE,
			.key = map_urid(NULL, "http://lv2plug.in/ns/ext/buf-size#minBlockLength"),
			.size = sizeof(uint32_t),
			.type = map_urid(NULL, "http://lv2plug.in/ns/ext/atom#Int"),
			.value = &MINIMUM_BLOCK_LENGTH,
		},
		{
			.context = LV2_OPTIONS_INSTANCE,
			.key = map_urid(NULL, "http://lv2plug.in/ns/ext/buf-size#maxBlockLength"),
			.size = sizeof(uint32_t),
			.type = map_urid(NULL, "http://lv2plug.in/ns/ext/atom#Int"),
			.value = &MAXIMUM_BLOCK_LENGTH,
		},
		{0},
	};
	const LV2_Feature options_feature = {
		.URI = "http://lv2plug.in/ns/ext/options#options",
		.data = (void *)options,
	};
	const LV2_Feature bounded_block_length_feature = {
		.URI = "http://lv2plug.in/ns/ext/buf-size#boundedBlockLength",
		.data = NULL,
	};
	const LV2_Feature supports_strict_bounds_feature = {
		.URI = "http://lv2plug.in/ns/ext/port-props#supportsStrictBounds",
		.data = NULL,
	};
	const LV2_Feature *features[5];
	features[0] = &urid_map_feature;
	features[1] = &bounded_block_length_feature;
	features[2] = &supports_strict_bounds_feature;
	features[3] = &options_feature;
	features[4] = NULL;

	LilvInstance *lilv_instance = lilv_plugin_instantiate(lilv_plugin, sample_rate, features);
	if (!lilv_instance) {
		fprintf(stderr, "Can't instantiate plugin '%s'\n", plugin_uri);
		return NULL;
	}
	uint32_t n_ports = lilv_plugin_get_num_ports(lilv_plugin);

	struct plugin *plugin = calloc(1, sizeof(*plugin) + n_ports * sizeof(plugin->ports[0]));
	if (!plugin) {
		perror("memory allocation failure");
		abort();
	}
	plugin->active = false;
	plugin->lilv_plugin = lilv_plugin;
	plugin->lilv_instance = lilv_instance;
	plugin->n_ports = n_ports;

	if (!initialize_ports(plugin)) {
		plugin_destroy(plugin);
		return NULL;
	}

	return plugin;
}

static bool parse_value(float *ret, const char *value, bool is_integer, bool is_toggle, bool is_enum,
			const LilvScalePoints *scale_points)
{
	if (is_enum) {
		const LilvNode *value_node = NULL;
		LILV_FOREACH(scale_points, iter, scale_points) {
			const LilvScalePoint *scale_point = lilv_scale_points_get(scale_points, iter);
			const LilvNode *label_node = lilv_scale_point_get_label(scale_point);
			if (strcmp(value, lilv_node_as_string(label_node)) == 0) {
				value_node = lilv_scale_point_get_value(scale_point);
				break;
			}
		}
		if (value_node) {
			*ret = lilv_node_as_float(value_node);
			return true;
		}
		char *end;
		errno = 0;
		unsigned long int_value = strtoul(value, &end, 10);
		if (errno == ERANGE) {
			fprintf(stderr, "Enum value too big: '%s'\n", value);
			return false;
		}
		if (end[0] != '\0') {
			fprintf(stderr, "Enum value not recognized: '%s'\n", value);
			return false;
		}
		*ret = (float)int_value;
		return true;
	}

	if (is_toggle) {
		if (strcmp(value, "false") == 0) {
			*ret = 0;
			return true;
		} else if (strcmp(value, "true") == 0) {
			*ret = 1;
			return true;
		} else if (strcmp(value, "0") == 0) {
			*ret = 0;
			return true;
		} else if (strcmp(value, "1") == 0) {
			*ret = 1;
			return true;
		}
		fprintf(stderr, "Unable to parse toggle value: '%s' (should be 'false', 'true', '0', '1')\n",
			value);
		return false;
	}

	if (is_integer) {
		char *end;
		errno = 0;
		unsigned long int_value = strtoul(value, &end, 0);
		if (errno == ERANGE) {
			fprintf(stderr, "Integer value too big: '%s'\n", value);
			return false;
		}
		if (end[0] != '\0') {
			fprintf(stderr, "Found junk (%s) while parsing integer value '%s'\n", end, value);
			return false;
		}
		*ret = (float)int_value;
		return true;
	}

	char *end;
	errno = 0;
	float float_value = strtof(value, &end);
	if (errno == ERANGE) {
		fprintf(stderr, "Underflow or overflow while parsing float value '%s'\n", value);
		return false;
	}
	if (end[0] != '\0') {
		if (strcmp(end, " dB") != 0) {
			fprintf(stderr, "Found junk (%s) while parsing float value '%s'\n", end, value);
			return false;
		}
		float_value = db_to_factor(float_value);
	}
	*ret = float_value;
	return true;
}

bool plugin_configure_ports(struct plugin *plugin, const struct key_value_pair *options, size_t n_options)
{
	plugin_reset_ports(plugin);
	for (size_t i = 0; i < n_options; i++) {
		const char *key = options[i].key;
		const char *value = options[i].value;
		if (strlen(key) == 0 || strlen(value) == 0) {
			fprintf(stderr, "Empty key or value (key='%s', value='%s')\n", key, value);
			return false;
		}
		LilvNode *symbol = lilv_new_string(world, key);
		const LilvPort *lilv_port = lilv_plugin_get_port_by_symbol(plugin->lilv_plugin, symbol);
		lilv_node_free(symbol);
		if (!lilv_port) {
			fprintf(stderr, "Unknown port '%s'\n", key);
			return false;
		}
		uint32_t index = lilv_port_get_index(plugin->lilv_plugin, lilv_port);
		assert(index < plugin->n_ports);
		struct port *port = &plugin->ports[index];
		if (port->direction != DIRECTION_INPUT) {
			fprintf(stderr, "Can't set option: Port '%s' is not an input port\n", key);
			return false;
		}
		if (port->type != TYPE_CONTROL) {
			fprintf(stderr, "Can't set option: Port '%s' is not a control port\n", key);
			return false;
		}

		float control;
		if (!parse_value(&control, value, port->is_integer, port->is_toggle, port->is_enum,
				 port->scale_points)) {
			return false;
		}
		if (control < port->min || control > port->max) {
			fprintf(stderr, "Value '%f' out of range for port '%s' (range=[%f, %f])\n",
				control, key, port->min, port->max);
			return false;
		}
		port->port_data.control = control;
	}
	return true;
}

void plugin_reset_state(struct plugin *plugin)
{
	if (plugin->active) {
		lilv_instance_deactivate(plugin->lilv_instance);
		plugin->active = false;
	}
}

void plugin_run(struct plugin *plugin, const float *in[2], float *out[2], uint32_t n_samples)
{
	if (n_samples == 0) {
		return;
	}
	assert(n_samples >= MINIMUM_BLOCK_LENGTH);
	assert(n_samples <= MAXIMUM_BLOCK_LENGTH);
	if (!plugin->active) {
		lilv_instance_activate(plugin->lilv_instance);
		plugin->active = true;
	}
	lilv_instance_connect_port(plugin->lilv_instance, plugin->input_port_indices[0], (float *)in[0]);
	lilv_instance_connect_port(plugin->lilv_instance, plugin->input_port_indices[1], (float *)in[1]);
	lilv_instance_connect_port(plugin->lilv_instance, plugin->output_port_indices[0], out[0]);
	lilv_instance_connect_port(plugin->lilv_instance, plugin->output_port_indices[1], out[1]);
	lilv_instance_run(plugin->lilv_instance, n_samples);
}

void plugin_destroy(struct plugin *plugin)
{
	for (uint32_t i = 0; i < plugin->n_ports; i++) {
		struct port *port = &plugin->ports[i];
		if (port->scale_points) {
			lilv_scale_points_free(port->scale_points);
		}
	}
	if (plugin->active) {
		lilv_instance_deactivate(plugin->lilv_instance);
	}
	lilv_instance_free(plugin->lilv_instance);
	free(plugin);
}

bool plugins_init(void)
{
	world = lilv_world_new();
	if (!world) {
		fprintf(stderr, "lilv_world_new failed: %s", errno != 0 ? strerror(errno) : "unknown error");
		return false;
	}
	lilv_world_load_all(world);
	plugins = lilv_world_get_all_plugins(world);
	if (!plugins) {
		fprintf(stderr, "lilv_world_get_all_plugins failed: %s", errno != 0 ? strerror(errno) : "unknown error");
		return false;
	}
	return true;
}

static array_t(const char *) uris;

static int compare_uris(const void *a, const void *b)
{
	return strcmp(*((const char **)a), *((const char **)b));
}

static LV2_URID map_urid(LV2_URID_Map_Handle handle, const char *uri)
{
	size_t idx;
	if (array_bsearch_index(uris, &uri, compare_uris, &idx)) {
		return (LV2_URID)idx;
	}
	char *uri_copy = strdup(uri);
	if (!uri_copy) {
		perror("memory allocation failure");
		abort();
	}
	array_insert(uris, idx, uri_copy);
	return (LV2_URID)idx;
}

void plugins_deinit(void)
{
	array_foreach_value(uris, uri) {
		free((char *)uri);
	}
	array_free(uris);
	lilv_world_free(world);
	plugins = NULL;
	world = NULL;
}
