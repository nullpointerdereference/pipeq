#define _GNU_SOURCE
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <lilv-0/lilv/lilv.h>

static void dump_ports(const LilvPlugin *plugin)
{
	uint32_t num_ports = lilv_plugin_get_num_ports(plugin);
	float min_values[num_ports], max_values[num_ports], def_values[num_ports];
	lilv_plugin_get_port_ranges_float(plugin, min_values, max_values, def_values);
	for (size_t i = 0; i < num_ports; i++) {
		const LilvNodes *nodes;
		const LilvPort *port = lilv_plugin_get_port_by_index(plugin, i);
		const char *name = lilv_node_as_string(lilv_port_get_name(plugin, port));
		const char *symbol = lilv_node_as_string(lilv_port_get_symbol(plugin, port));
		float min = min_values[i];
		float max = max_values[i];
		float def = def_values[i];
		const char *direction = NULL;
		const char *type = NULL;
		bool __attribute__((unused)) is_array = false;
		bool __attribute__((unused)) strict_bounds = false;
		bool __attribute__((unused)) enumeration = false;
		bool __attribute__((unused)) integer = false;
		bool __attribute__((unused)) logarithmic = false;
		bool __attribute__((unused)) reports_latency = false;
		bool __attribute__((unused)) optional = false;
		bool __attribute__((unused)) toggled = false;
		bool __attribute__((unused)) not_automatic = false;
		bool __attribute__((unused)) momentary_trigger = false;
		bool __attribute__((unused)) is_side_chain = false;
		bool __attribute__((unused)) not_on_gui = false;
		bool __attribute__((unused)) non_automable = false;
		bool __attribute__((unused)) expensive = false;
		nodes = lilv_port_get_classes(plugin, port);
		LILV_FOREACH(nodes, iter, nodes) {
			const LilvNode *node = lilv_nodes_get(nodes, iter);
			const char *class = lilv_node_as_string(node);
			if (strcmp(class, "http://lv2plug.in/ns/lv2core#InputPort") == 0) {
				direction = "input";
			} else if (strcmp(class, "http://lv2plug.in/ns/lv2core#OutputPort") == 0) {
				direction = "output";
			} else if (strcmp(class, "http://lv2plug.in/ns/lv2core#AudioPort") == 0) {
				type = "float";
				is_array = true;
			} else if (strcmp(class, "http://lv2plug.in/ns/lv2core#ControlPort") == 0) {
				type = "float";
			} else if (strcmp(class, "http://lv2plug.in/ns/ext/atom#AtomPort") == 0) {
				type = "Atom";
			} else if (strcmp(class, "http://lv2plug.in/ns/lv2core#CVPort") == 0) {
				type = "float";
				is_array = true;
			} else {
				fprintf(stderr, "Unknown class: %s\n", class);
				assert(false);
			}
		}
		nodes = lilv_port_get_properties(plugin, port);
		LILV_FOREACH(nodes, iter, nodes) {
			const LilvNode *node = lilv_nodes_get(nodes, iter);
			const char *property = lilv_node_as_string(node);
			if (strcmp(property, "http://lv2plug.in/ns/ext/port-props#hasStrictBounds") == 0) {
				strict_bounds = true;
			} else if (strcmp(property, "http://lv2plug.in/ns/lv2core#enumeration") == 0) {
				enumeration = true;
			} else if (strcmp(property, "http://lv2plug.in/ns/lv2core#integer") == 0) {
				integer = true;
			} else if (strcmp(property, "http://lv2plug.in/ns/ext/port-props#logarithmic") == 0) {
				logarithmic = true;
			} else if (strcmp(property, "http://lv2plug.in/ns/lv2core#reportsLatency") == 0) {
				reports_latency = true;
			} else if (strcmp(property, "http://lv2plug.in/ns/lv2core#connectionOptional") == 0) {
				optional = true;
			} else if (strcmp(property, "http://lv2plug.in/ns/lv2core#toggled") == 0) {
				toggled = true;
			} else if (strcmp(property, "http://lv2plug.in/ns/ext/port-props#notAutomatic") == 0) {
				not_automatic = true;
			} else if (strcmp(property, "http://lv2plug.in/ns/ext/port-props#trigger") == 0) {
				momentary_trigger = true;
			} else if (strcmp(property, "http://lv2plug.in/ns/lv2core#isSideChain") == 0) {
				is_side_chain = true;
			} else if (strcmp(property, "http://lv2plug.in/ns/ext/port-props#notOnGUI") == 0) {
				not_on_gui = true;
			} else if (strcmp(property, "http://kxstudio.sf.net/ns/lv2ext/props#NonAutomable") == 0) {
				non_automable = true;
			} else if (strcmp(property, "http://lv2plug.in/ns/ext/port-props#expensive") == 0) {
				expensive = true;
			} else {
				fprintf(stderr, "Unknown property: %s\n", property);
				assert(false);
			}
		}

		if (enumeration) {
			type = "enum";
		} else if (integer) {
			type = "int";
		}

		printf("# Port #%zu \"%s\" default=%g range=[%g, %g] %s%s %s%s%s%s\n",
		       i, name, def, min, max, type, is_array ? "[]" : "", direction,
		       optional ? " optional" : "", logarithmic ? " logarithmic" : "",
		       toggled ? " toggled" : "");

		if (enumeration) {
			const LilvScalePoints *scale_points = lilv_port_get_scale_points(plugin, port);
			printf("#      { ");
			bool first = true;
			LILV_FOREACH(scale_points, iter, scale_points) {
				const LilvScalePoint *scale_point = lilv_scale_points_get(scale_points, iter);
				const LilvNode *label = lilv_scale_point_get_label(scale_point);
				const LilvNode *value = lilv_scale_point_get_value(scale_point);
				if (!first) {
					printf(", ");
				}
				first = false;
				printf("%s: %s", lilv_node_as_string(label), lilv_node_as_string(value));
			}
			printf(" }\n");
		}

		if (strcmp(direction, "input") != 0 || is_array || strcmp(type, "Atom") == 0) {
			continue;
		}

		printf("%s%s: %g\n", optional ? "# " : "", symbol, def);
	}
}

static void dump_features(const LilvPlugin *plugin)
{
	LilvNodes *required = lilv_plugin_get_required_features(plugin);
	LILV_FOREACH(nodes, i, required) {
		const LilvNode *node = lilv_nodes_get(required, i);
		const char *uri = lilv_node_as_uri(node);
		printf("# required: %s\n", uri);
	}
	lilv_nodes_free(required);
	LilvNodes *optional = lilv_plugin_get_optional_features(plugin);
	LILV_FOREACH(nodes, i, optional) {
		const LilvNode *node = lilv_nodes_get(optional, i);
		const char *uri = lilv_node_as_uri(node);
		printf("# optional: %s\n", uri);
	}
	lilv_nodes_free(optional);
}

int main(int argc, char **argv)
{
	LilvWorld *world = lilv_world_new();
	lilv_world_load_all(world);
	const LilvPlugins *plugins = lilv_world_get_all_plugins(world);

	LILV_FOREACH(plugins, i, plugins) {
		const LilvPlugin *plugin = lilv_plugins_get(plugins, i);
		LilvNode *name_node = lilv_plugin_get_name(plugin);
		const char *name = lilv_node_as_string(name_node);

		bool match = true;
		for (int i = 1; i < argc; i++) {
			if (!strcasestr(name, argv[i])) {
				match = false;
			}
		}
		if (!match) {
			continue;
		}

		printf("# %s\n", name);
		const LilvNode *uri_node = lilv_plugin_get_uri(plugin);
		const char *uri = lilv_node_as_string(uri_node);
		printf("[%s]\n", uri);
		if (argc > 1) {
			dump_features(plugin);
			dump_ports(plugin);
		}
		puts("\n");
	}
	return 0;

}
