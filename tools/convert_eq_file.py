#! /usr/bin/env python3

import sys

def eat(string, s):
    string = string.strip()
    assert(string.startswith(s))
    string = string[len(s):]
    return string.strip()

f = open(sys.argv[1], "r")
print("[http://lsp-plug.in/plugins/lv2/para_equalizer_x32_stereo]")
for l in f:
    l = l.strip(" \n")
    if l.startswith("Preamp:"):
        l = eat(l, "Preamp:")
        gain, l = l.split(" ", 1)
        gain = float(gain)
        l = eat(l, "dB")
        print("g_in:", gain, "dB")
        print("g_out:", -gain, "dB")
    elif l.startswith("Filter"):
        l = eat(l, "Filter")
        i, l = l.split(":")
        i = int(i) - 1
        assert(i >= 0 and i <= 31)
        l = eat(l, "ON")
        assert(l.startswith("PK")) # TODO implement other filter types
        filter_type = "Bell"
        l = eat(l, "PK")
        l = eat(l, "Fc")
        freq, l = l.split(" ", 1)
        freq = int(freq)
        l = eat(l, "Hz")
        l = eat(l, "Gain")
        gain, l = l.split(" ", 1)
        gain = float(gain)
        l = eat(l, "dB")
        l = eat(l, "Q")
        q = float(l)
        print(f"ft_{i}: {filter_type}")
        print(f"fm_{i}: APO (DR)")
        print(f"f_{i}: {freq}")
        print(f"g_{i}: {gain} dB")
        print(f"q_{i}: {q}")
